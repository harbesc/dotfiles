#!/bin/bash
set -e

while :
do
  password=$(cat ~/.decaf)
  echo "----------------------------------------------------------"
  echo "----------------------------------------------------------"
  echo "----------------------------------------------------------"
  echo "DUO push necessary to authenticate!!!! Check your phone!!!"
  echo "----------------------------------------------------------"
  echo "----------------------------------------------------------"
  echo "----------------------------------------------------------"
  ./decaf__expect.sh "$password"
  cat ~/.aws/credentials > ~/.fog
  sleep 10
  vi -s ini_to_yaml ~/.fog
  echo "Sleeping an hour..."
  sleep 3600
done
