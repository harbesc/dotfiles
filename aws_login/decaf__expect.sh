#!/usr/bin/expect -f

set password [lindex $argv 0]
spawn aws_key_gen login -a DECAF -r arn:aws:iam::736855434844:role/User --force

expect "Enter your password for DECAF*" { 
	send "$password\r"
	expect "Passcode or option*" { send "1\r" }
	}
